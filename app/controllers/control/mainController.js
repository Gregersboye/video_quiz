app.controller('controlController', [
    '$scope',
    'socket',
    'competitors',
    'videos',
    'quizService',
    function(
        $scope,
        socket,
        competitors,
        videos,
        quizService
    ){
        $scope.counting= {started: false};
        $scope.currentVideo = 0;
        $scope.log =[];
        $scope.competitors = competitors;

        $scope.videos = videos;
        $scope.video = $scope.videos[$scope.currentVideo];

        socket.on('joined quiz', function(msg){
            $scope.competitors.push(msg);
        });

        socket.on('log', function(msg){
            $scope.log.push(msg);
        });

        socket.on('videoState', function(state){
            $scope.video.state = state;

        });

        $scope.continueVideo = function(){
            socket.emit('videoState', 'continued');

        };

        $scope.nextVideo = function(){
            socket.emit('next video');
            $scope.currentVideo++;

            reset();
        };

        $scope.continueBtnState = function(){
                return $scope.video.state !== 'paused';
            
           

        };

        $scope.showResults = function(){
            socket.emit('show results');
        };

        $scope.revealResult = function(){
            console.log("hahaha");
            socket.emit('reveal result');
        };


        $scope.getCompetitorBackground = function(competitor){
            if(competitor.hasAnswered){
                return (competitor.answer == $scope.videos[$scope.currentVideo].Correct) ? 'list-group-item-success' : 'list-group-item-danger';
            }

            return "";

        };

        $scope.startQuiz = function(){
            $scope.counting.started = true;
            quizService.startQuiz().then(function(result){

            });

        };

        function reset(){
            $scope.video = $scope.videos[$scope.currentVideo];
            $scope.video.state = "playing";
            $scope.answers = 0;
            angular.forEach($scope.competitors,function(competitor){
                competitor.hasAnswered= false;
                competitor.showAnswer =false;
                competitor.answer = null
            });
        }

        reset();

        socket.on("hasAnswered", function(answer){
            var competitor = $scope.competitors[answer.competitor];
            var numAnswer = parseInt(answer.answer);
            competitor.hasAnswered = true;

            competitor.answer = numAnswer;
            competitor.Answers.push(numAnswer);
            if(numAnswer === $scope.videos[$scope.currentVideo].Correct){
                competitor.Point++;
            }
            $scope.answers++;
        });

        $scope.showAnswers = function(){
           socket.emit('log', 'show answers');
        };

    }]);
