app.controller('viewVideoController', ['$scope', 'quizService', 'socket', 'videoService', 'localStorageService', function($scope, quizService,socket,videoService, localStorageService){


	$scope.competitor = localStorageService.get('Competitor');


	socket.on('showOptions',function(data){
		$scope.competitor.answered = false;
		$scope.video = data;
	});

	socket.on('reset video', function(){
		reset();
	});

	function reset(){
		$scope.video = null;
		$scope.answered = false;
	}

	reset();

	$scope.sendAnswer = function(){

		var data = {
			id: $scope.competitor.Id,
			answer:$scope.video.answer
		};

		videoService.sendAnswer(data).then(function(result){
			$scope.answered = true;
		});
	};
}]);