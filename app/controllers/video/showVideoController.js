app.controller('showVideoController', [
	'$scope',
	'video',
	'socket',
	'videoService',
	"competitors",
	"logService",
	"$state",
	function(
		$scope,
		video,
		socket,
		videoService,
		competitors,
        logService,
		$state
		){
	 $scope.video=video;
	 

	 $scope.competitors=competitors;
	 socket.emit('videoState', 'playing');

	 function reset(){
         $scope.video.state = "playing";
         $scope.answers = 0;
		 angular.forEach($scope.competitors,function(competitor){
			competitor.hasAnswered= false;
			competitor.showAnswer =false;
			competitor.answer = null
		 });
     }

     reset();
	 $scope.getLabel = function($index){

	 	var competitor = $scope.competitors[$index];
	 	if(competitor.showAnswer) {

            if (competitor.answer === $scope.video.Correct) {
                return "right";
            } else {
                return "wrong";
            }
        }

	 	return "fat";
	 };

	 socket.on("hasAnswered", function(answer){

	 	$scope.competitors[answer.competitor].hasAnswered = true;
	 	$scope.competitors[answer.competitor].answer = parseInt(answer.answer);
	 	$scope.answers++;
	 });

	 socket.on('show results', function(){
        $state.go('quiz.results');
     });

	 socket.on('videoState', function(state){
	     $scope.video.state = state;

	     if(state === "continued"){
	 	    player.play();
         }
         if(state === "paused"){
	         player.pause();
         }
	 });

	 socket.on('log', function(message){
	     if(message === 'show answers'){
	        showAnswers();
         }
        });

	 function showAnswers(){
	 	var i = 0;

		var showTime = setInterval(function() {
            $scope.$apply(function() {
                $scope.competitors[i].showAnswer = true;
        	});

			if (i+1 === $scope.competitors.length) {
				clearInterval(showTime);
				socket.emit('log', 'Answers shown');
			}else{
                i++;
			}

		}, 1000);
	 }

	 socket.on('reset video', function(result){

			$scope.video = result;
            reset();
	 });

	var player = document.getElementById('videoPlayer');
	if(typeof player !== "undefined" ){
		player.addEventListener("timeupdate", function(){

		    if(this.currentTime >= $scope.video.BreakTime && $scope.video.state === "playing") {

				$scope.video.state = "paused";
				socket.emit('videoState', 'paused');
				this.pause();

		    }
		});

		player.addEventListener("ended", function(){

				socket.emit('videoState', 'ended');



		});
	}


}]);