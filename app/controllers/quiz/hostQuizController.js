app.controller('hostQuizController', ['$scope','$state', 'quizService', 'socket', 'competitors', function($scope,$state, quizService,socket, competitors){
	$scope.competitors = competitors;
	$scope.counting= {started: false};
	socket.on('joined quiz', function(msg){
		$scope.competitors.push(msg);
 	});

	socket.on("update counter", function(data){
		$scope.counting = {
			started: true,
			time: data
		};
	});

    socket.on('watch video', function(){
        $state.go("video.show");
    });



}]);