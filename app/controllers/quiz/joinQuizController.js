app.controller('joinQuizController', ['$scope', 'quizService', 'socket', '$state','localStorageService', function($scope, quizService, socket, $state,localStorageService){
	$scope.contender = (localStorageService.get("Competitor") === null) ? {Name: ""} : localStorageService.get("Competitor")

	if($scope.contender.Id !== null){
		$scope.joined =true;
	}


	$scope.joined = false;

	socket.on('quiz started', function(msg){

		quizStarted = true;

	});

	$scope.counting = {
		started : false,
		time : 30
	};

	socket.on("update counter", function(data){
		$scope.counting = {
			started: true,
			time: data
		};
	});

	socket.on('watch video', function(data){
		$state.go("video.view");
	});

	$scope.joinQuiz = function(){
		
		quizService.joinQuiz($scope.contender).then(function(data){
			if(data !== null){
				var saveResult = localStorageService.set('Competitor', data);
				$scope.joined = true;
			}else{
				alert("quizzen er desværre startet");
			}

		});	





	}



	

}])