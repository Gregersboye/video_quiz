/**
 * Created by gregers on 5/12/17.
 */
app.controller('resultQuizController', [
    '$scope',
    'socket',
    'competitors',
    'orderByFilter',

    function(
        $scope,
        socket,
        competitors,
        orderBy

    ){

    $scope.competitors = orderBy(competitors, 'point', true);
    var counter = $scope.competitors.length-1;
    angular.forEach($scope.competitors,function(competitor){
        competitor.showPoints = false;
        competitor.show = false;

    });


    socket.on('reveal results', function(){

        console.log(counter);
            $scope.competitors[counter].show = true;


        counter--;
    });

}]);