app.service("logService", ["$http", "socket", function($http, socket){
    this.log = function(message){
        socket.emit('log', message);
    };
}]);