app.service('videoService', ['$http', function($http){
	this.getCurrentVideo = function(){
		return $http.get("/api/videos/current").then(function(result){
			return result.data;
		});
	};

	this.sendQuestion = function(){
		return $http.post("/api/video/paused").then(function(){

		});
	};

	this.sendAnswer = function(data){
		return $http.post("/api/video/answer",data).then(function(result){
			return result.data;
		});

	};

	this.getVideos = function(){
		return $http.get('/api/video').then(function(result){
			return result.data;
		})
	};

	this.advanceVideo = function(){
		return $http.post("/api/video/next").then(function(result){
			return result.data;
		});
	};
}]);