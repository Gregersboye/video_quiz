app.service("quizService", ["$http", function($http){

	this.getCompetitors = function(){
		return $http.get("/api/competitors").then(function(result){
			
			return result.data;
		});
	};

	this.joinQuiz = function(contender){
		return $http.post("/api/quiz/join", contender).then(function(result){
			return result.data;
		});
	};

	this.startQuiz = function(){
		return $http.post("/api/quiz/start").then(function(result){
			return result.data;
		});
	};

}])