var app = angular.module('videoApp', [
"ui.router",

"LocalStorageModule",
'ngMaterial'
	]);



app.config(function ($stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider,localStorageServiceProvider) {

    localStorageServiceProvider
    .setPrefix('videoApp');

    $urlMatcherFactoryProvider.strictMode(false);
     $urlRouterProvider.otherwise("/"+defaultRoute);
   
    $stateProvider
        .state("quiz", {
            url:"/",
            abstract: true,
            template: "<div ui-view></div>"
        })
        .state('quiz.join', {
            url: "join",
            controller: "joinQuizController",
            templateUrl: "/app/views/quiz/join.html"

        })
        .state('quiz.host', {
            url: "host",
            controller: "hostQuizController",
            templateUrl: "/app/views/quiz/host.html",
            resolve: {
                competitors: ["quizService", function(quizService){
                    return quizService.getCompetitors();
                }]
            }
        })
        .state('quiz.results', {
            url: "results",
            controller:"resultQuizController",
            templateUrl: "/app/views/quiz/results.html",
            resolve: {
                competitors: ["quizService", function (quizService) {
                    return quizService.getCompetitors();
                }]
            }
        })
        .state('control', {
            url: "/control",
            controller: "controlController",
            templateUrl: "/app/views/control/main.html",
            resolve: {
                competitors: ["quizService", function(quizService){
                    return quizService.getCompetitors();
                }],
                videos: ['videoService', function(videoService){
                    return videoService.getVideos();
                }]
            }
        })

        .state('video', {
            abstract: true,
            url: "/video",
            template:"<ui-view />"
        })
        .state('video.show',{
            url: "/show",
            controller:"showVideoController",
            templateUrl:"/app/views/video/show.html",
            resolve:{
                video:["videoService", function(videoService){
                    return videoService.getCurrentVideo();
                }],
                competitors: ["quizService", function(quizService){
                    return quizService.getCompetitors();
                }]

            }
        })

        .state('video.view', {
            url: "/view",
            controller:"viewVideoController",
            templateUrl:"/app/views/video/view.html"
        });
    
});