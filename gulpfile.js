var gulp = require("gulp");
var concat = require('gulp-concat');

gulp.task('scripts', function() {
    return gulp.src('app/**/*.js')
      .pipe(concat('main.js'))
      .pipe(gulp.dest('public/app'));
});

gulp.task('watch', function() {
  // Watch .js files
  gulp.watch('app/**/*.js', ['scripts']);
});

gulp.task('default', ['scripts','watch']);