var express = require('express');
var path = require('path');
var videos = require("./videos.json");
var app = express();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var bodyParser = require('body-parser');
var publicPath = path.resolve(__dirname, 'public');
var competitors = [];
var quizStarted  = false;
var currentVideo = 0;

 app.use(express.static(publicPath));
 app.use( bodyParser.json() ); 


app.get('/', function(req, res){
    res.sendFile(__dirname + '/index.html');
});

app.get('/api/video', function(req, res){
    res.send(videos);
});

app.get('/control', function(req, res){
	res.sendFile(__dirname + '/control.html');
});


app.get('/host', function(req, res){
    res.sendFile(__dirname + '/host.html');
});

app.post("/api/quiz/start", function(req, res){
	startVideo();
	res.send("ok");
});

function startVideo() {
    log("Countdown started");
    var timeLeft = 3;
    var timerId = setInterval(function(){
        if (timeLeft < 0) {
            log("Countdown ended");
            io.emit("Log", "");
            clearTimeout(timerId);
            io.emit("watch video", videos[currentVideo]);

        } else {
            io.emit("update counter", timeLeft);
            timeLeft -= 0.1;
        }
    }, 100);
    io.emit("start quiz", true);
    //quizStarted = true;
}

function log(message){
    console.log(message);
    io.emit('log', message);
}

app.post("/api/quiz/join", function(req, res){
	if (!quizStarted) {
		var newCompetitor = {Name: req.body.Name,  Id: competitors.length, Point: 0, Answers:[]};
		competitors.push(newCompetitor);
		io.emit("joined quiz", newCompetitor);
		log("user '"+req.body.Name+"' joined the quiz");
    	res.send(newCompetitor);
	}else{
		res.send(null);
	}
});

app.post('/api/log', function(req, res){
    log(req.body.message);
    res.send('ok');
});

app.get("/api/competitors", function(req, res){
	res.send(competitors);
});

app.post("/api/video/answer",function(req,res){
    var answer  = parseInt(req.body.answer);
	var competitorId = req.body.id;
    log('User #'+competitorId+' has answered on video #'+currentVideo+': '+answer);
	competitors[competitorId].Answers[currentVideo] = answer;
	if (answer === videos[currentVideo].Correct) {
		competitors[competitorId].Point++;
	}
	io.emit("hasAnswered", {'competitor': competitorId, 'answer': answer});

	res.send(true);

});



app.get("/api/videos/current",function(req, res){
	res.send(videos[currentVideo]);
});

io.on('connection', function(socket){

    socket.on('show results', function(){
        log("go to results");
        io.emit('show results');
    });

    socket.on('reveal result', function(){
        log('reveal another result');
        io.emit('reveal results');
    });

    socket.on('next video', function(){
        log("next video started");
        currentVideo++;
        //startVideo();
        io.emit('reset video', videos[currentVideo]);
    });

    socket.on('log', function(msg){
        log(msg);
    });

    socket.on('videoState',function(state){
		log('Video state: '+state);
		io.emit('videoState', state);

		if(state === "paused"){
            var video = videos[currentVideo];
            io.emit("showOptions",{question:video.Question,answers:video.Answers});

        }
    });
});

var port = 8080;

http.listen(port, function(){
  log('listening on *:'+port);
});