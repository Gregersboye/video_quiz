var app = angular.module('videoApp', [
"ui.router",

"LocalStorageModule",
'ngMaterial'
	]);



app.config(function ($stateProvider, $urlRouterProvider, $urlMatcherFactoryProvider,localStorageServiceProvider) {

    localStorageServiceProvider
    .setPrefix('videoApp');

    $urlMatcherFactoryProvider.strictMode(false);
     $urlRouterProvider.otherwise("/"+defaultRoute);
   
    $stateProvider
        .state("quiz", {
            url:"/",
            abstract: true,
            template: "<div ui-view></div>"
        })
        .state('quiz.join', {
            url: "join",
            controller: "joinQuizController",
            templateUrl: "/app/views/quiz/join.html"

        })
        .state('quiz.host', {
            url: "host",
            controller: "hostQuizController",
            templateUrl: "/app/views/quiz/host.html",
            resolve: {
                competitors: ["quizService", function(quizService){
                    return quizService.getCompetitors();
                }]
            }
        })
        .state('quiz.results', {
            url: "results",
            controller:"resultQuizController",
            templateUrl: "/app/views/quiz/results.html",
            resolve: {
                competitors: ["quizService", function (quizService) {
                    return quizService.getCompetitors();
                }]
            }
        })
        .state('control', {
            url: "/control",
            controller: "controlController",
            templateUrl: "/app/views/control/main.html",
            resolve: {
                competitors: ["quizService", function(quizService){
                    return quizService.getCompetitors();
                }],
                videos: ['videoService', function(videoService){
                    return videoService.getVideos();
                }]
            }
        })

        .state('video', {
            abstract: true,
            url: "/video",
            template:"<ui-view />"
        })
        .state('video.show',{
            url: "/show",
            controller:"showVideoController",
            templateUrl:"/app/views/video/show.html",
            resolve:{
                video:["videoService", function(videoService){
                    return videoService.getCurrentVideo();
                }],
                competitors: ["quizService", function(quizService){
                    return quizService.getCompetitors();
                }]

            }
        })

        .state('video.view', {
            url: "/view",
            controller:"viewVideoController",
            templateUrl:"/app/views/video/view.html"
        });
    
});
function Quiz(){

	this.Teams = [];
	this.Videos = []; 
	
}
function Team(){
	this.Name = "";
	this.Members = [];
	this.Points = 0;
}
var video = function(){
	var Title = "";
	var Src ="";

	var title = "";
	var BreakTime= "";
	var Question = "";
	var Answers = [];
	var Correct = 0;
}
app.factory('teamFactory', [function () {
	

	return function(data) {
		var team = new Team();
		team.Name = data.Name;

		return team;

	};
}])
app.factory('socket', function ($rootScope) {
  var socket = io.connect();
  return {
    on: function (eventName, callback) {
      socket.on(eventName, function () {  
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },   
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      });
    }
  };
});
app.filter("trustUrl", ['$sce', function ($sce) {
        return function (recordingUrl) {
            return $sce.trustAsResourceUrl(recordingUrl);
        };
    }]);
app.service("logService", ["$http", "socket", function($http, socket){
    this.log = function(message){
        socket.emit('log', message);
    };
}]);
app.service("quizService", ["$http", function($http){

	this.getCompetitors = function(){
		return $http.get("/api/competitors").then(function(result){
			
			return result.data;
		});
	};

	this.joinQuiz = function(contender){
		return $http.post("/api/quiz/join", contender).then(function(result){
			return result.data;
		});
	};

	this.startQuiz = function(){
		return $http.post("/api/quiz/start").then(function(result){
			return result.data;
		});
	};

}])
app.service('videoService', ['$http', function($http){
	this.getCurrentVideo = function(){
		return $http.get("/api/videos/current").then(function(result){
			return result.data;
		});
	};

	this.sendQuestion = function(){
		return $http.post("/api/video/paused").then(function(){

		});
	};

	this.sendAnswer = function(data){
		return $http.post("/api/video/answer",data).then(function(result){
			return result.data;
		});

	};

	this.getVideos = function(){
		return $http.get('/api/video').then(function(result){
			return result.data;
		})
	};

	this.advanceVideo = function(){
		return $http.post("/api/video/next").then(function(result){
			return result.data;
		});
	};
}]);
app.controller('controlController', [
    '$scope',
    'socket',
    'competitors',
    'videos',
    'quizService',
    function(
        $scope,
        socket,
        competitors,
        videos,
        quizService
    ){
        $scope.counting= {started: false};
        $scope.currentVideo = 0;
        $scope.log =[];
        $scope.competitors = competitors;

        $scope.videos = videos;
        $scope.video = $scope.videos[$scope.currentVideo];

        socket.on('joined quiz', function(msg){
            $scope.competitors.push(msg);
        });

        socket.on('log', function(msg){
            $scope.log.push(msg);
        });

        socket.on('videoState', function(state){
            $scope.video.state = state;

        });

        $scope.continueVideo = function(){
            socket.emit('videoState', 'continued');

        };

        $scope.nextVideo = function(){
            socket.emit('next video');
            $scope.currentVideo++;

            reset();
        };

        $scope.continueBtnState = function(){
                return $scope.video.state !== 'paused';
            
           

        };

        $scope.showResults = function(){
            socket.emit('show results');
        };

        $scope.revealResult = function(){
            console.log("hahaha");
            socket.emit('reveal result');
        };


        $scope.getCompetitorBackground = function(competitor){
            if(competitor.hasAnswered){
                return (competitor.answer == $scope.videos[$scope.currentVideo].Correct) ? 'list-group-item-success' : 'list-group-item-danger';
            }

            return "";

        };

        $scope.startQuiz = function(){
            $scope.counting.started = true;
            quizService.startQuiz().then(function(result){

            });

        };

        function reset(){
            $scope.video = $scope.videos[$scope.currentVideo];
            $scope.video.state = "playing";
            $scope.answers = 0;
            angular.forEach($scope.competitors,function(competitor){
                competitor.hasAnswered= false;
                competitor.showAnswer =false;
                competitor.answer = null
            });
        }

        reset();

        socket.on("hasAnswered", function(answer){
            var competitor = $scope.competitors[answer.competitor];
            var numAnswer = parseInt(answer.answer);
            competitor.hasAnswered = true;

            competitor.answer = numAnswer;
            competitor.Answers.push(numAnswer);
            if(numAnswer === $scope.videos[$scope.currentVideo].Correct){
                competitor.Point++;
            }
            $scope.answers++;
        });

        $scope.showAnswers = function(){
           socket.emit('log', 'show answers');
        };

    }]);

app.controller('hostQuizController', ['$scope','$state', 'quizService', 'socket', 'competitors', function($scope,$state, quizService,socket, competitors){
	$scope.competitors = competitors;
	$scope.counting= {started: false};
	socket.on('joined quiz', function(msg){
		$scope.competitors.push(msg);
 	});

	socket.on("update counter", function(data){
		$scope.counting = {
			started: true,
			time: data
		};
	});

    socket.on('watch video', function(){
        $state.go("video.show");
    });



}]);
app.controller('joinQuizController', ['$scope', 'quizService', 'socket', '$state','localStorageService', function($scope, quizService, socket, $state,localStorageService){
	$scope.contender = (localStorageService.get("Competitor") === null) ? {Name: ""} : localStorageService.get("Competitor")

	if($scope.contender.Id !== null){
		$scope.joined =true;
	}


	$scope.joined = false;

	socket.on('quiz started', function(msg){

		quizStarted = true;

	});

	$scope.counting = {
		started : false,
		time : 30
	};

	socket.on("update counter", function(data){
		$scope.counting = {
			started: true,
			time: data
		};
	});

	socket.on('watch video', function(data){
		$state.go("video.view");
	});

	$scope.joinQuiz = function(){
		
		quizService.joinQuiz($scope.contender).then(function(data){
			if(data !== null){
				var saveResult = localStorageService.set('Competitor', data);
				$scope.joined = true;
			}else{
				alert("quizzen er desværre startet");
			}

		});	





	}



	

}])
/**
 * Created by gregers on 5/12/17.
 */
app.controller('resultQuizController', [
    '$scope',
    'socket',
    'competitors',
    'orderByFilter',

    function(
        $scope,
        socket,
        competitors,
        orderBy

    ){

    $scope.competitors = orderBy(competitors, 'point', true);
    var counter = $scope.competitors.length-1;
    angular.forEach($scope.competitors,function(competitor){
        competitor.showPoints = false;
        competitor.show = false;

    });


    socket.on('reveal results', function(){

        console.log(counter);
            $scope.competitors[counter].show = true;


        counter--;
    });

}]);
app.controller('showVideoController', [
	'$scope',
	'video',
	'socket',
	'videoService',
	"competitors",
	"logService",
	"$state",
	function(
		$scope,
		video,
		socket,
		videoService,
		competitors,
        logService,
		$state
		){
	 $scope.video=video;
	 

	 $scope.competitors=competitors;
	 socket.emit('videoState', 'playing');

	 function reset(){
         $scope.video.state = "playing";
         $scope.answers = 0;
		 angular.forEach($scope.competitors,function(competitor){
			competitor.hasAnswered= false;
			competitor.showAnswer =false;
			competitor.answer = null
		 });
     }

     reset();
	 $scope.getLabel = function($index){

	 	var competitor = $scope.competitors[$index];
	 	if(competitor.showAnswer) {

            if (competitor.answer === $scope.video.Correct) {
                return "right";
            } else {
                return "wrong";
            }
        }

	 	return "fat";
	 };

	 socket.on("hasAnswered", function(answer){

	 	$scope.competitors[answer.competitor].hasAnswered = true;
	 	$scope.competitors[answer.competitor].answer = parseInt(answer.answer);
	 	$scope.answers++;
	 });

	 socket.on('show results', function(){
        $state.go('quiz.results');
     });

	 socket.on('videoState', function(state){
	     $scope.video.state = state;

	     if(state === "continued"){
	 	    player.play();
         }
         if(state === "paused"){
	         player.pause();
         }
	 });

	 socket.on('log', function(message){
	     if(message === 'show answers'){
	        showAnswers();
         }
        });

	 function showAnswers(){
	 	var i = 0;

		var showTime = setInterval(function() {
            $scope.$apply(function() {
                $scope.competitors[i].showAnswer = true;
        	});

			if (i+1 === $scope.competitors.length) {
				clearInterval(showTime);
				socket.emit('log', 'Answers shown');
			}else{
                i++;
			}

		}, 1000);
	 }

	 socket.on('reset video', function(result){

			$scope.video = result;
            reset();
	 });

	var player = document.getElementById('videoPlayer');
	if(typeof player !== "undefined" ){
		player.addEventListener("timeupdate", function(){

		    if(this.currentTime >= $scope.video.BreakTime && $scope.video.state === "playing") {

				$scope.video.state = "paused";
				socket.emit('videoState', 'paused');
				this.pause();

		    }
		});

		player.addEventListener("ended", function(){

				socket.emit('videoState', 'ended');



		});
	}


}]);
app.controller('viewVideoController', ['$scope', 'quizService', 'socket', 'videoService', 'localStorageService', function($scope, quizService,socket,videoService, localStorageService){


	$scope.competitor = localStorageService.get('Competitor');


	socket.on('showOptions',function(data){
		$scope.competitor.answered = false;
		$scope.video = data;
	});

	socket.on('reset video', function(){
		reset();
	});

	function reset(){
		$scope.video = null;
		$scope.answered = false;
	}

	reset();

	$scope.sendAnswer = function(){

		var data = {
			id: $scope.competitor.Id,
			answer:$scope.video.answer
		};

		videoService.sendAnswer(data).then(function(result){
			$scope.answered = true;
		});
	};
}]);